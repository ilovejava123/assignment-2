/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment2;

/**
 * A Member class to store information about a member name, weight, height and
 * BMI value, and also list of member's activities.
 *
 * @author khookashing(B1500970)
 */
public class Member {

    // instance variables
    private String name;
    private double weightInKg;
    private double heightInM;
    private ActivityList activities;

    // default constructor with no args
    public Member() {
        name = "Not set";
        weightInKg = 0.0;
        heightInM = 0.0;
        activities = new ActivityList(5);
    }

    // constructor with argument 
    public Member(String inName, double inWeightInKg, double inHeightInM) {
        if (!inName.equals("")) {
            name = inName;
        } else {
            name = "not set";
        }

        if (inWeightInKg > 0) {
            weightInKg = inWeightInKg;
        } else {
            weightInKg = 0.0;
        }

        if (inHeightInM > 0) {
            heightInM = inHeightInM;
        } else {
            heightInM = 0.0;
        }

        activities = new ActivityList(5);
    }

    // getters
    public String getName() {
        return name;
    }

    public double getWeightInKg() {
        return weightInKg;
    }

    public double getHeightInM() {
        return heightInM;
    }

    public ActivityList getActivityList() {
        return activities;
    }

    // setters
    public void setName(String newName) {
        if (!newName.equals("")) {
            name = newName;
        } else {
            name = "not set";
        }
    }

    public void setWeightInKg(double newWeightInKg) {
        if (newWeightInKg > 0) {
            weightInKg = newWeightInKg;
        } else {
            weightInKg = 0.0;
        }
    }

    public void setHeightInM(double newHeightInM) {
        if (newHeightInM > 0) {
            heightInM = newHeightInM;
        } else {
            heightInM = 0.0;
        }
    }

    public void setActivityList(ActivityList newActivities) {
        activities = newActivities;
    }

    // display information about the member
    public String toString() {
        return "\nName: " + name
                + "\nWeight: " + weightInKg + "kg"
                + "\nHeight: " + heightInM + "m";
    }

    // calculate BMI
    public double getBMI() {
        return weightInKg / (heightInM * heightInM);
    }
    
    // calculate the calories burn
    public double calcCaloriesBurn() {
        double caloriesBurn = 0;
        for (int i = 0; i < activities.getNumOfActivity(); i++) {
            caloriesBurn += activities.calcMETxDuration(i) * weightInKg;
        }

        return caloriesBurn;
    }
    
    // compare name
    // return true if found
    public boolean findMbActivity(String name, Activity inActivity) {
        Activity findName = activities.findActivity(name);
        if (findName == inActivity) {
            return true;
        }
        return false;
    }
}