/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment2;

/**
 * A container class that manages a list of Member.
 * 
 * @author ongjiacheng(B1302698)
 */
public class MemberList {

    // instance variable
    private final int SIZE = 5;     // assume Club can only have 5 member
    private Member[] member;        //an array of member objects
    private int numberOfMember;

    // constructor which sets to default size
    public MemberList() {
        member = new Member[SIZE];
        numberOfMember = 0;
    }

    // constructor which sets to the size pass in
    public MemberList(int size) {
        if (size <= 0) {
            member = new Member[SIZE];
        } else {
            member = new Member[size];
        }
        numberOfMember = 0;
    }

    // method to add a member
    // return false if the list is full
    public boolean addMember(Member newMember) {
        if (numberOfMember == member.length) {
            return false;
        }
        member[numberOfMember++] = newMember;
        return true;
    }

    // a method to find a member
    // return null if not found
    public Member findMember(String name) {
        for (int i = 0; i < numberOfMember; i++) {
            if (member[i].getName().equalsIgnoreCase(name)) {
                return member[i];
            }
        }
        return null;
    }

    // a method to find the highest BMI
    // and return the object
    public Member findHighestBMI() {
        Member highestBMI = member[0];
        for (int i = 1; i < numberOfMember; i++) {
            if (member[i].getBMI() > highestBMI.getBMI()) {
                highestBMI = member[i];
            }
        }
        return highestBMI;
    }

    // a method to calculate the average of all member weight
    // in all member
    public double calAverageWeight() {
        double total = 0.0;
        double ave = 0.0;
        for (int i = 0; i < numberOfMember; i++) {
            total += member[i].getWeightInKg();
        }
        ave = total / numberOfMember;
        return ave;
    }
    
    // a method to calculate the average of all member height
    // in all member
    public double calAverageHeight() {
        double total = 0.0;
        double ave = 0.0;
        for (int i = 0; i < numberOfMember; i++) {
            total += member[i].getHeightInM();
        }
        ave = total / numberOfMember;
        return ave;
    }

    // a method to show all member in the member list
    public String getAll() {
        String str = "All members : \n";
        for (int i = 0; i < numberOfMember; i++) {
            str += i + 1 + ". " + member[i].getName() + "\n";
        }
        return str;
    }

    // a method to show all detail of all members in the list
    public String mbDetail() {
        String str = "All members: \n";
        for (int i = 0; i < numberOfMember; i++) {
            str += i + 1 + ". " + member[i].getName() + " has height of " + member[i].getHeightInM() + "m and weight of " + member[i].getWeightInKg() + "kg.\n";
        }
        return str;
    }

    // a method to return the number of members
    public int getNumOfMember() {
        return numberOfMember;
    }
}