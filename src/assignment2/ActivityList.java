/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment2;

/**
 * A container class that manages a list of Activity.
 *
 * @author khookashing(B1500970)
 */
public class ActivityList {
    
    // instance variable
    private final int size = 5;     // assume Club can only have 5 activities
    private Activity[] activity;    // an array of activity objects
    private int numOfActivity;

    // constructor which sets to default size
    public ActivityList() {
        activity = new Activity[size];
        numOfActivity = 0;
    }

    // constructor which sets to size passed in
    public ActivityList(int newSize) {
        if (newSize > 0) {
            activity = new Activity[newSize];
        } else {
            activity = new Activity[size];
        }
        numOfActivity = 0;
    }

    // method to add an activity
    // returns false if the list is full
    public boolean addActivity(Activity newActivity) {
        if (numOfActivity == activity.length) {
            return false;
        }
        activity[numOfActivity++] = newActivity;
        return true;
    }

    // a method to find an activity
    // return null if not found
    public Activity findActivity(String name) {
        for (int i = 0; i < numOfActivity; i++) {
            if (activity[i].getName().equalsIgnoreCase(name)) {
                return activity[i];
            }
        }
        return null;
    }

    // a method to calculate the average of MET
    // in all activity
    public double calcAverageMET() {
        double sum = 0.0;
        for (int i = 0; i < numOfActivity; i++) {
            sum += activity[i].getMET();
        }
        return sum / numOfActivity;
    }

    // a method to find highest total cost
    // and return the object
    public Activity findHighestCost() {
        Activity highestCost = activity[0];
        for (int i = 1; i < numOfActivity; i++) {
            if (activity[i].totalCost() > highestCost.totalCost()) {
                highestCost = activity[i];
            }
        }
        return highestCost;
    }

    // a method to calculate the total cost of all activities
    public double findTotalCost() {
        double totalCost = 0;
        for (int i = 0; i < numOfActivity; i++) {
            totalCost += activity[i].totalCost();
        }
        return totalCost;
    }

    // a method to calculate MET times Duration
    public double calcMETxDuration(int num) {
        double total = 0;
        total = activity[num].getDuration() * activity[num].getMET();
        return total;
    }
    
    // a method to show all activity in the activity list
    public String getAll() {
        String str = "All activities: \n";
        for (int i = 0; i < numOfActivity; i++) {
            str += i + 1 + ". " + activity[i].getName() + "\n";
        }
        return str;
    }
    
    // a method to show all detail of all activities in the list
    public String actiDetail() {
        String str = "All available activities: \n";
        for (int i = 0; i < numOfActivity; i++) {
            str += i + 1 + ". " + "The activity, " + activity[i].getName() + " has MET value of " + activity[i].getMET() + " and total cost RM" + activity[i].totalCost() + ".\n";
        }
        return str;
    }
    
    // a method to return the number of activities
    public int getNumOfActivity() {
        return numOfActivity;
    }
}