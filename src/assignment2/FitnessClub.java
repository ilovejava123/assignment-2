/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment2;

/**
 * This program provide a menu for the fitness club.
 * 
 * @author ongjiacheng(B1302698) & khookashing(B1500970) 
 * 3/4/2016
 */
import java.util.*;

public class FitnessClub {

    public static Scanner sc = new Scanner(System.in);
    public static ActivityList acti;
    public static MemberList mb;

    public static void main(String[] args) {
        acti = new ActivityList();
        mb = new MemberList();
        int menuChoice;

        do {
            System.out.println("Welcome to ILoveJava123 fitness club!!");
            System.out.println("What would you like to do?");
            System.out.println("1. Manage Activity");
            System.out.println("2. Manage Member");
            System.out.println("3. Record Activity for Member");
            System.out.println("4. Quit.");
            System.out.println("");
            System.out.println("What is your choice: ");
            menuChoice = sc.nextInt();
            sc.nextLine();
            switch (menuChoice) {
                case 1:
                    manageActi();
                    break;
                case 2:
                    manageMember();
                    break;
                case 3:
                    if (mb.getNumOfMember() == 0 || acti.getNumOfActivity() == 0) {
                        if (mb.getNumOfMember() > 0) {
                            System.out.println("There is no activity added yet!!");
                            System.out.println("");
                        } else if (acti.getNumOfActivity() > 0) {
                            System.out.println("There is no member added yet!!");
                            System.out.println("");
                        } else {
                            System.out.println("There is no activity or member added yet!!");
                            System.out.println("");
                        }
                    } else {
                        recordActivity();
                    }
                    break;
                case 4:
                    System.out.println("");
                    System.out.println("Thank You and GoodBye!!");
                    break;
                default:
                    System.out.println("Invalid choice.");
                    System.out.println("");
                    break;
            }

        } while (menuChoice != 4);
    }
    
    // menu for managing activity
    public static void manageActi() {
        int actiChoice;
        do {
            System.out.println("");
            System.out.println("Managing activity: ");
            System.out.println("1. Add activity to list (max 5).");
            System.out.println("2. Show all activities.");
            System.out.println("3. Display the average MET of all activities.");
            System.out.println("4. Display activity with the highest total cost.");
            System.out.println("5. Update duration or cost of an activity.");
            System.out.println("6. Back to previous menu.");
            System.out.println("");
            System.out.println("What is your choice: ");
            actiChoice = sc.nextInt();
            sc.nextLine();
            switch (actiChoice) {
                case 1:
                    addActivity();
                    break;
                case 2: {
                    if (acti.getNumOfActivity() == 0) {
                        System.out.println("There is no activity added yet!!");
                    } else {
                        System.out.println(acti.actiDetail());
                    }
                }
                break;
                case 3:
                    if (acti.getNumOfActivity() == 0) {
                        System.out.println("There is no activity added yet!!");
                    } else {
                        displayAverageMET();
                    }
                    break;
                case 4:
                    if (acti.getNumOfActivity() == 0) {
                        System.out.println("There is no activity added yet!!");
                    } else {
                        displayHighestTotalCost();
                    }
                    break;
                case 5:
                    if (acti.getNumOfActivity() == 0) {
                        System.out.println("There is no activity added yet!!");
                    } else {
                        updateActivity();
                    }
                    break;
                case 6:
                    System.out.println("");
                    return;
                default:
                    System.out.println("Invalid choice.");
                    break;
            }

        } while (actiChoice != 6);
    }

    // adding activity to the activity list
    public static void addActivity() {
        System.out.println("");
        System.out.println("Adding a new activity: ");
        String actiName = getUserInputName(1);
        

        System.out.println("Please enter the MET value (max 15.0): ");
        double actiMET = sc.nextDouble();
        sc.nextLine();
        while (actiMET <= 0 || actiMET > 15) {
            System.out.println("Invalid MET value.");
            System.out.println("Please enter the MET value between (max 15.0): ");
            actiMET = sc.nextDouble();
            sc.nextLine();
        }
        double actiDuration = getActivityDur();
        double actiCostperHour = getActivityCost();
        
        Activity newActivity = new Activity(actiName, actiMET, actiDuration, actiCostperHour);
        if (acti.addActivity(newActivity)) {
            System.out.println("");
            System.out.println("The activity, " + newActivity.getName() + " is added.");
        } else {
            System.out.println("Activity not added, activity list is full!!");
        }
    }
    
    // display average MET of all activity
    public static void displayAverageMET() {
        double averageMETvalue = acti.calcAverageMET();

        System.out.printf("The average MET value of all activity is %.2f\n", averageMETvalue);
    }

    // display the activity with the highest total cost
    public static void displayHighestTotalCost() {
        Activity highestTotalCost = acti.findHighestCost();

        System.out.printf("The activity with the highest total cost is %s with RM%.2f\n ", highestTotalCost.getName(), highestTotalCost.totalCost());
    }
    
    // update the duration or cost of an activity given by the name
    public static void updateActivity() {
        System.out.println("");
        System.out.println("Which activity do you want to update?");
        System.out.println(acti.getAll());
        String wantedActiName = getUserInputName(1);

        Activity actiFound = acti.findActivity(wantedActiName);
        if (actiFound == null) {
            System.out.println("Activity not found.");
        } else {
            System.out.println("Activity found: \n" + actiFound.toString());
            System.out.println("");
            System.out.println("Would you like to :");
            System.out.println("1. Change duration");
            System.out.println("2. Change cost");
            System.out.println("");
            System.out.println("Your choice: ");
            int choice = sc.nextInt();
            sc.nextLine();
            switch (choice) {
                case 1:
                    System.out.println("Enter the new duration: ");
                    double newDur = getActivityDur();
                    actiFound.setDuration(newDur);
                    System.out.println("Duration changed.");
                    break;
                case 2:
                    System.out.println("Enter the new cost: ");
                    double newCost = getActivityCost();
                    actiFound.setCost(newCost);
                    System.out.println("Cost changed.");
                    break;
                default:
                    System.out.println("Invalid Choice.");
                    break;
            }
        }
    }
    
    // menu for managing member
    public static void manageMember() {
        int memberChoice;
        do {
            System.out.println("");
            System.out.println("1. Add member to list (max 5).");
            System.out.println("2. Show all member.");
            System.out.println("3. Display the average height and weight of all Member.");
            System.out.println("4. Display member with the highest BMI.");
            System.out.println("5. Update height or weight of a member.");
            System.out.println("6. Back to previous menu.");
            System.out.println("");
            System.out.println("What is your choice: ");
            memberChoice = sc.nextInt();
            sc.nextLine();
            switch (memberChoice) {
                case 1:
                    addMember();
                    break;
                case 2:
                    if (mb.getNumOfMember() == 0) {
                        System.out.println("There is no member added yet!!");
                    } else {
                        System.out.println(mb.mbDetail());
                    }
                    break;
                case 3:
                    if (mb.getNumOfMember() == 0) {
                        System.out.println("There is no member added yet!!");
                    } else {
                        displayAverage();
                    }
                    break;
                case 4:
                    if (mb.getNumOfMember() == 0) {
                        System.out.println("There is no member added yet!!");
                    } else {
                        displayHighestBMI();
                    }
                    break;
                case 5:
                    if (mb.getNumOfMember() == 0) {
                        System.out.println("There is no member added yet!!");
                    } else {
                        updateMember();
                    }
                    break;
                case 6:
                    return;
                default:
                    System.out.println("Invalid choice.");
                    System.out.println("");
                    break;
            }
        } while (memberChoice != 6);
    }
    
    // adding member to member list
    public static void addMember() {
        System.out.println("Adding a new member: ");
        String mbName = getUserInputName(2);

        double mbHeight = getMemberHeight();

        double mbWeight = getMemberWeight();

        Member newMember = new Member(mbName, mbWeight, mbHeight);

        if (mb.addMember(newMember)) {
            System.out.println("A new member name, " + newMember.getName() + " is added.");
        } else {
            System.out.println("Member not added, member list is full.");
        }
    }
    
    // display the average weight and average height of all member
    public static void displayAverage() {
        double aveHeight;
        double aveWeight;

        aveHeight = mb.calAverageHeight();
        aveWeight = mb.calAverageWeight();

        System.out.printf("The average height of all member is : %.2f metre. \n", aveHeight);
        System.out.printf("The average weight of all member is : %.2f Kg. \n", aveWeight);
    }

    // display member with the highest BMI
    public static void displayHighestBMI() {
        Member highestBMI = mb.findHighestBMI();

        System.out.printf("The member with the highest BMI is %s with BMI of %.2f \n", highestBMI.getName(), highestBMI.getBMI());
    }

    // update the height or weight of a member given by the name
    public static void updateMember() {
        System.out.println("");
        System.out.println("Which member to update?");
        System.out.println(mb.getAll());
        String wantedMemberName = getUserInputName(3);
        Member mbFound = mb.findMember(wantedMemberName);
        if (mbFound == null) {
            System.out.println("Member not found.");
        } else {
            System.out.println("Member found: " + mbFound.toString());
            System.out.println("");
            System.out.println("Would you like to: ");
            System.out.println("1. Change Height");
            System.out.println("2. Change Weight");
            System.out.println("");
            System.out.println("Your choice: ");
            int choice = sc.nextInt();
            sc.nextLine();
            switch (choice) {
                case 1:
                    System.out.println("The new height for " + wantedMemberName + ": ");
                    double newHeight = getMemberHeight();
                    mbFound.setHeightInM(newHeight);
                    System.out.println("The height has changed.");
                    break;
                case 2:
                    System.out.println("The new weight for " + wantedMemberName + ": ");
                    double newWeight = getMemberWeight();
                    mbFound.setWeightInKg(newWeight);
                    System.out.println("The weight has changed.");
                    break;
                default:
                    System.out.println("Invalid Choice.");
                    break;
            }
        }
    }

    // menu for recording activities for a member
    public static void recordActivity() {
        int recordChoice;

        String foundMember = getUserInputName(3);
        Member wantedMember = mb.findMember(foundMember);

        if (wantedMember == null) {
            System.out.println("Member not found.");
        } else {
            System.out.println("");
            System.out.println("Member found!!");
            do {
                System.out.println("");
                System.out.println("Menu list for " + wantedMember.getName());
                System.out.println("1. Add activity for a member");
                System.out.println("2. Display the BMI of the member");
                System.out.println("3. Display the total cost of all activities of the member");
                System.out.println("4. Display the calories burnt for the member");
                System.out.println("5. Back to previous menu");
                System.out.println("");
                System.out.println("What is your choice: ");
                recordChoice = sc.nextInt();
                sc.nextLine();
                switch (recordChoice) {
                    case 1:
                        addActivityForMember(wantedMember);
                        break;
                    case 2:
                        if (wantedMember.getActivityList().getNumOfActivity() == 0) {
                            System.out.println("There is no activity added to member yet!!");
                        } else {
                            showMemberBMI(wantedMember);
                        }
                        break;
                    case 3:
                        if (wantedMember.getActivityList().getNumOfActivity() == 0) {
                            System.out.println("There is no activity added to member yet!!");
                        } else {
                            showTotalCost(wantedMember);
                        }
                        break;
                    case 4:
                        if (wantedMember.getActivityList().getNumOfActivity() == 0) {
                            System.out.println("There is no activity added to member yet!!");
                        } else {
                            showCaloriesBurn(wantedMember);
                        }
                        break;
                    case 5:
                        System.out.println("");
                        return;
                    default:
                        System.out.println("Invalid choice.");
                        System.out.println("");
                        break;
                }

            } while (recordChoice != 5);
        }
    }
    
    // adding activity for a member given from the activity list
    public static void addActivityForMember(Member inMember) {
        boolean activityHaveDone = true;
        String menu = acti.getAll();
        String menuChoice;
        int numberOfActivity = acti.getNumOfActivity();
        if (numberOfActivity == 0) {
            System.out.println("There is no activity added.");
            System.out.println("Please add an activity to the activity list.");
        } else {
            System.out.println("");
            System.out.println(menu);
            System.out.println("Please select an activity: ");
            System.out.println("");
            System.out.println("Enter the name of the activity: ");
            menuChoice = sc.nextLine();

            Activity wantedActivity = acti.findActivity(menuChoice);
            if (wantedActivity == null) {
                System.out.println("Activity not found!");
            } else {
                activityHaveDone = inMember.findMbActivity(menuChoice, wantedActivity);
                if (activityHaveDone == true) {
                    System.out.println("You have added the activity already.");
                    System.out.println("Please choose another activity.");
                } else {
                    inMember.getActivityList().addActivity(wantedActivity);
                    System.out.println("Activity added!!");
                }
            }
        }
    }
    
    // display the BMI of the member
    public static void showMemberBMI(Member inMember) {
        System.out.printf("%s has a BMI of %.2f.\n", inMember.getName(), inMember.getBMI());
    }

    // diplay the total cost of all the activities done by the member
    public static void showTotalCost(Member inMember) {
        System.out.printf("%s has done activity with a total cost of %.2f.\n", inMember.getName(), inMember.getActivityList().findTotalCost());
    }

    // display the total calories burned of the member
    public static void showCaloriesBurn(Member inMember) {
        System.out.printf("%s has burned total calories of %.2f.\n", inMember.getName(), inMember.calcCaloriesBurn());
    }

    // validation for when getting input of name from the user
    public static String getUserInputName(int i) {
        String name = null;
        switch (i) {
            case 1:
                System.out.println("Please enter the name of the activity: ");
                name = sc.nextLine();
                while (name.equals("")) {
                    System.out.println("Name cannot be empty.");
                    System.out.println("Please enter the name of the activity: ");
                    name = sc.nextLine();
                }
                break;
            case 2:
                System.out.println("Please enter the name of the member: ");
                name = sc.nextLine();
                while (name.equals("")) {
                    System.out.println("Name cannot be empty.");
                    System.out.println("Please enter the name of the member: ");
                    name = sc.nextLine();
                }
                break;
            default:
                System.out.println("Please enter the member name you wish to find: ");
                name = sc.nextLine();
                while (name.equals("")) {
                    System.out.println("Name cannot be empty.");
                    System.out.println("Please enter the member name you wish to find: ");
                    name = sc.nextLine();
                }
                break;
        }
        return name;
    }
    
    // validation for duration
    public static double getActivityDur() {
        System.out.println("Please enter the duration in hours (max 6.0 hours): ");
        double actiDuration = sc.nextDouble();
        sc.nextLine();
        while (actiDuration <= 0 || actiDuration > 6) {
            System.out.println("Invalid duration value.");
            System.out.println("Please enter the duration in hours (max 6.0 hours): ");
            actiDuration = sc.nextDouble();
            sc.nextLine();
        }
        return actiDuration;
    }

    // validation for cost
    public static double getActivityCost() {
        System.out.println("Please enter the cost per hours (max 50.0): ");
        double actiCostperHour = sc.nextDouble();
        sc.nextLine();
        while (actiCostperHour <= 0 || actiCostperHour > 50) {
            System.out.println("Invalid cost value.");
            System.out.println("Please enter the cost per hours (max 50.0): ");
            actiCostperHour = sc.nextDouble();
            sc.nextLine();
        }
        return actiCostperHour;
    }

    // validation for height
    public static double getMemberHeight() {
        System.out.println("Please enter the height in metre (max 2.5 metre): ");
        double mbHeight = sc.nextDouble();
        sc.nextLine();
        while (mbHeight <= 0 || mbHeight > 2.5) {
            System.out.println("Invalid height value.");
            System.out.println("Please enter the height in metre (max 2.5 metre): ");
            mbHeight = sc.nextDouble();
            sc.nextLine();
        }
        return mbHeight;
    }

    // validation for weight
    public static double getMemberWeight() {
        System.out.println("Please enter the weight in kg (max 250 Kg): ");
        double mbWeight = sc.nextDouble();
        sc.nextLine();
        while (mbWeight <= 0 || mbWeight > 250) {
            System.out.println("Invalid weight value.");
            System.out.println("Please enter the weight in kg (max 250 Kg): ");
            mbWeight = sc.nextDouble();
            sc.nextLine();
        }
        return mbWeight;
    }
}
