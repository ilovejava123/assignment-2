/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment2;

/**
 * An activity class to store information about an activity name, MET value,
 * duration and cost.
 *
 * @author ongjiacheng(B1302698)
 */
public class Activity {

    // instance variable
    private String activityName;
    private double MET;
    private double durationInHours;
    private double costPerHour;

    // default constructor with no args
    public Activity() {
        activityName = "Not Set";
        MET = 0.0;
        durationInHours = 0.0;
        costPerHour = 0.0;
    }

    // constructor with args
    public Activity(String inName, double inMET, double inDuration, double inCost) {
        if (!"".equals(inName)) {
            activityName = inName;
        } else {
            activityName = "Not Set";
        }

        if (inMET > 0) {
            MET = inMET;
        } else {
            MET = 0.0;
        }

        if (inDuration > 0) {
            durationInHours = inDuration;
        } else {
            durationInHours = 0.0;
        }

        if (inCost > 0) {
            costPerHour = inCost;
        } else {
            costPerHour = 0.0;
        }
    }

    // getters
    public String getName() {
        return activityName;
    }

    public double getMET() {
        return MET;
    }

    public double getDuration() {
        return durationInHours;
    }

    public double getCost() {
        return costPerHour;
    }

    // setters
    public void setName(String newName) {
        if (!"".equals(newName)) {
            activityName = newName;
        } else {
            activityName = "Not Set";
        }
    }

    public void setMET(double newMET) {
        if (newMET > 0) {
            MET = newMET;
        } else {
            MET = 0.0;
        }
    }

    public void setDuration(double newDuration) {
        if (newDuration > 0) {
            durationInHours = newDuration;
        } else {
            durationInHours = 0.0;
        }
    }

    public void setCost(double newCost) {
        if (newCost > 0) {
            costPerHour = newCost;
        } else {
            costPerHour = 0.0;
        }
    }

    // display information about the activity
    public String toString() {
        return "\nActivity Name: " + activityName
                + "\nMET: " + MET
                + "\nDuration: " + durationInHours + " hours "
                + "\nCost: RM" + costPerHour;
    }

    // calculate the total cost of the activity
    public double totalCost() {
        return costPerHour * durationInHours;
    }
}